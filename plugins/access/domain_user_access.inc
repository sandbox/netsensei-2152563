<?php

/**
 * @file
 * Plugin to provide access control based upon active domain.
 */


$plugin = array(
  'title' => t('Domain User Access'),
  'description' => t('Control access by active domain.'),
  'callback' => 'ipe_domain_access_domain_user_access_check',
  'summary' => 'ipe_domain_access_domain_user_acesss_summary',
  'required context' => new ctools_context_required(t('User'), 'user'),
);

/**
 * Check for access based on the currently active domain.
 */
function ipe_domain_access_domain_user_access_check($conf, $context, $plugin) {
  $_domain = domain_get_domain();
  $user_context = $context->data;

  $domain_id = $_domain['domain_id'];
  if (isset($user_context->domain_user[$domain_id])) {
    return TRUE;
  }

  return FALSE;
}

/**
 * Provide a summary description based upon the checked domains.
 */
function ipe_domain_access_domain_user_acesss_summary($conf, $context, $plugin) {
  return t('Content is only available if the active domain is assigned to the user');
}
